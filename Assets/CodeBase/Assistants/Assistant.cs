using CodeBase.InteractObjects;
using InteractObjects;
using NTC.Global.Cache;
using Tables;
using UnityEngine;

namespace CodeBase.Assistants
{
    public class Assistant : MonoCache
    {
        [SerializeField] private AssistantInteract _interact;
        [SerializeField] private AssistantMovement _movement;
        [SerializeField] private AssistantCharacteristics _characteristics;

        private SpawnerResources _spawner;
        private DestroyerResources _destroyer;
        private StorageResources _storage;
    
        private Transform _spawnerTransform;
        private Transform _destroyerTransform;
        private Transform _storageTransform;

        private TablesScripts.Tables _tables;
        private Table _dirtyTable;
        private Transform _tableTransform;
    
        private bool _initialize;
        private bool _goToStorage;

        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            _spawner = Establishment.Instance.SpawnerResources;
            _destroyer = Establishment.Instance.DestroyerResources;
            _storage = Establishment.Instance.StorageResources;
            _tables = TablesScripts.Tables.Instance;

            _spawnerTransform = _spawner.PlaceForStartInteract;
            _destroyerTransform = _destroyer.PlaceForStartInteract;
            _storageTransform = _storage.PlaceForStartInteract;
            
            _characteristics.Initialize(_movement, _interact);

            _initialize = true;
        }

        protected override void LateRun()
        {
            if (!_initialize) return;

            StateMachine();
        }

        private void StateMachine()
        {
            if (!_goToStorage && _tables.DirtyTable && 
                (_dirtyTable == null || !_dirtyTable.Unharvested && !_interact.MaxResources))
            {
                _dirtyTable = _tables.ClearTheTable(transform.position);
                if (_dirtyTable != null) _tableTransform = _dirtyTable.transform;
            }
            else if (_dirtyTable != null)
            {
                if (_dirtyTable.Unharvested && _interact.CountResources == 0)
                {
                    if (_movement.Move(_tableTransform.position) && _interact.CountResources == 0)
                    {
                        _dirtyTable.Cleaning(_interact);
                    }
                }
                else if (_interact.CountResources != 0)
                {
                    if (_movement.Move(_destroyerTransform.position))
                    {
                        _destroyer.StartInteract(_interact);
                    }
                }
                else _dirtyTable = null;
            }
            else if (!_goToStorage && _interact.CountResources > 0 && (_spawner.CountResources == 0 || _interact.MaxResources))
            {
                _goToStorage = true;
            }
            else if (_goToStorage && _interact.CountResources > 0)
            {
                if (_movement.Move(_storageTransform.position))
                {
                    _storage.StartInteract(_interact);

                    if (_interact.CountResources == 0)
                    {
                        _goToStorage = false;
                    }
                }
            }
            else if (_spawner.CountResources > 0)
            {
                if (_movement.Move(_spawnerTransform.position))
                {
                    _spawner.StartInteract(_interact);
                }
            }
        }

        public void UpCharacteristics(CharacteristicsType type)
        {
            _characteristics.UpCharacteristics(type);
        }
    }
}