﻿using System;
using NTC.Global.Cache;
using UnityEngine;

namespace CodeBase.Assistants
{
    public class AssistantCharacteristics : MonoCache
    {
        private AssistantInteract _interact;
        private AssistantMovement _movement;
        
        private int _moveSpeedLevel;
        private int _capacityLevel;

        internal void Initialize(AssistantMovement movement, AssistantInteract interact)
        {
            _movement = movement;
            _interact = interact;
            
            UpCharacteristics(CharacteristicsType.MoveSpeed);
            UpCharacteristics(CharacteristicsType.Capacity);
        }
        
        internal void UpCharacteristics(CharacteristicsType type)
        {
            var value = GetUpValuesForCharacteristic(type);

            if (PlayerPrefs.GetInt("Level" + type) <= 0 ||
                PlayerPrefs.GetInt("Level" + type) > 5) 
                return;

            switch (type)
            {
                case CharacteristicsType.MoveSpeed:
                    _movement.UpMoveSpeed(value);
                    break;
                case CharacteristicsType.Capacity:
                    _interact.UpCapacity(value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private int GetUpValuesForCharacteristic(CharacteristicsType type)
        {
            var value = 0;
        
            switch (type)
            {
                case CharacteristicsType.MoveSpeed when _moveSpeedLevel >= 5:
                    throw new ArgumentOutOfRangeException();
                case CharacteristicsType.MoveSpeed:
                    _moveSpeedLevel = PlayerPrefs.GetInt("Level" + CharacteristicsType.MoveSpeed);

                    value = _moveSpeedLevel switch
                    {
                        1 => 4,
                        2 => 5,
                        3 => 6,
                        4 => 7,
                        5 => 10,
                        _ => value
                    };

                    break;
                case CharacteristicsType.Capacity when _capacityLevel >= 5:
                    throw new ArgumentOutOfRangeException();
                case CharacteristicsType.Capacity:
                    _capacityLevel = PlayerPrefs.GetInt("Level" + CharacteristicsType.Capacity);

                    value = _capacityLevel switch
                    {
                        1 => 3,
                        2 => 5,
                        3 => 7,
                        4 => 10,
                        5 => 15,
                        _ => value
                    };

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            return value;
        }
    }
}