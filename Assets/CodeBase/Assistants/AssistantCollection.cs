﻿using System.Collections.Generic;
using NTC.Global.Cache;
using NTC.Global.Pool;
using UnityEngine;

namespace CodeBase.Assistants
{
    public class AssistantCollection : MonoCache
    {
        private List<Assistant> _assistants = new List<Assistant>();

        public static AssistantCollection Instance { get; private set; }
    
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            int amount = PlayerPrefs.GetInt("Level" + CharacteristicsType.Employ);

            for (int i = 0; i < amount; i++)
            {
                Spawn();
            }
        }

        private void Spawn()
        {
            var transform1 = transform;
            
            Assistant assistant = NightPool.Spawn(Prefabs.Instance.Assistant, transform1.position + transform1.forward);
        
            _assistants.Add(assistant);
        }

        public void UpCharacteristics(CharacteristicsType type)
        {
            if(PlayerPrefs.GetInt("Level" + type) > Constants.MaxHrCharactersLevel) return;

            if (type == CharacteristicsType.Employ)
            {
                Spawn();
                
                return;
            }
        
            int amount = _assistants.Count;

            for (int i = 0; i < amount; i++)
            {
                _assistants[i].UpCharacteristics(type);
            }
        }
    }
}