﻿using System;
using CodeBase.Customers;
using UnityEngine;

namespace CodeBase.Assistants
{
    public class AssistantMovement : AIMovement
    {
        internal void UpMoveSpeed(int moveSpeed)
        {
            if (_agent.speed > moveSpeed) 
                throw new ArgumentOutOfRangeException();
            
            _agent.speed = moveSpeed;
        }
    }
}