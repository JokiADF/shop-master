﻿using CodeBase.Canvas;
using CodeBase.InteractObjects;
using CodeBase.PlayerScripts;
using InteractObjects;
using UnityEngine;

namespace CodeBase.Assistants
{
    public class Upgrades : InteractWithMoneyObject
    {
        [SerializeField] private UpgradesType upgradesType;
        
        private GameplayCanvas _gameplayCanvas;
        private AssistantCollection _assistantCollection;
        private PlayerCharacteristics _playerCharacteristics;

        private bool _isOpen;

        private void Start()
        {
            Initialize();
        }

        internal override void Initialize()
        {
            base.Initialize();
            
            if (PlayerPrefs.GetInt("UnlockUpgrades") == 0)
            {
                ResetUpgradesPrefs();
            }
            
            _gameplayCanvas = GameplayCanvas.Instance;
            _assistantCollection = AssistantCollection.Instance;
            _playerCharacteristics = Player.Instance.Characteristics;
            
            _gameplayCanvas.InitializeUpgradesButtons(UpCharacteristics, ResetInteractValues);
        }

        private static void ResetUpgradesPrefs()
        {
            PlayerPrefs.SetInt("UnlockUpgrades", 1);
            
            PlayerPrefs.SetInt("PriceBuy" + CharacteristicsType.MoveSpeed, 100);
            PlayerPrefs.SetInt("PriceBuy" + CharacteristicsType.Capacity, 100);
            PlayerPrefs.SetInt("PriceBuy" + CharacteristicsType.Employ, 100);

            PlayerPrefs.SetInt("Level" + CharacteristicsType.MoveSpeed, 0);
            PlayerPrefs.SetInt("Level" + CharacteristicsType.Capacity, 0);
            PlayerPrefs.SetInt("Level" + CharacteristicsType.Employ, 0);
            
            PlayerPrefs.SetInt("PriceBuy" + CharacteristicsType.MoveSpeedPlayer, 100);
            PlayerPrefs.SetInt("PriceBuy" + CharacteristicsType.CapacityPlayer, 100);
            PlayerPrefs.SetInt("PriceBuy" + CharacteristicsType.Income, 100);

            PlayerPrefs.SetInt("Level" + CharacteristicsType.MoveSpeedPlayer, 0);
            PlayerPrefs.SetInt("Level" + CharacteristicsType.CapacityPlayer, 0);
            PlayerPrefs.SetInt("Level" + CharacteristicsType.Income, 0);
        }

        private void UpCharacteristics(CharacteristicsType characteristicsType)
        {
            if(PlayerPrefs.GetInt("Money") < PlayerPrefs.GetInt("PriceBuy" + characteristicsType) ||
               PlayerPrefs.GetInt("Level" + characteristicsType) >= Constants.MaxHrCharactersLevel) return;

            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") - PlayerPrefs.GetInt("PriceBuy" + characteristicsType));
            PlayerPrefs.SetInt("PriceBuy" + characteristicsType, PlayerPrefs.GetInt("PriceBuy" + characteristicsType) + 50);

            PlayerPrefs.SetInt("Level" + characteristicsType, PlayerPrefs.GetInt("Level" + characteristicsType) + 1);
        

            if ((int)characteristicsType <= 2)
            {
                _assistantCollection.UpCharacteristics(characteristicsType);
            }
            else
            {
                _playerCharacteristics.UpCharacteristics(characteristicsType);
            }
        }

        protected override void Interact(WorkerResources workerResources)
        {
            if(_isOpen) return;
            
            _isOpen = true;
        
            _gameplayCanvas.OpenUpgrades(upgradesType);
        }
    
        protected override void ResetInteractValues()
        {
            if(!_isOpen) return;

            if (Helper.GetDistance(_playerTransform.position, PlaceForStartInteract.position) > _startDistance)
            {
                _isOpen = false;
            
                base.ResetInteractValues();
            }
        
            _gameplayCanvas.CloseUpgrades();
        }
    }

    public enum UpgradesType
    {
        Assistants,
        Player
    }

    public enum CharacteristicsType
    {
        MoveSpeed,
        Capacity,
        Employ,
        MoveSpeedPlayer,
        CapacityPlayer,
        Income
    }
}