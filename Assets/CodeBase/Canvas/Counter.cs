using NTC.Global.Cache;
using TMPro;
using UnityEngine;

public class Counter : MonoCache
{
    [SerializeField] private TextMeshProUGUI _text;

    public void UpdateCounter(int count)
    {
        _text.text = count.ToString();
    }
}
