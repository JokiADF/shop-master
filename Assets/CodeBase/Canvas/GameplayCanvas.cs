using System;
using CodeBase.Assistants;
using CodeBase.PlayerScripts;
using UnityEngine;

namespace CodeBase.Canvas
{
    public class GameplayCanvas : UICanvas
    {
        [SerializeField] private UpgradesUi upgradesUi;
        [SerializeField] private Counter moneyCounter;

        public static GameplayCanvas Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null) 
            {
                Instance = this;
            
                DontDestroyOnLoad(gameObject);        
            }
            else
            {
                Destroy(this);
            }
        }

        public void OpenUpgrades(UpgradesType upgradesType) => 
            upgradesUi.OpenPanel(upgradesType);

        public void CloseUpgrades() => 
            upgradesUi.ClosePanel();

        public void InitializeUpgradesButtons(Action<CharacteristicsType> upCharacteristics, Action exit) =>
            upgradesUi.InitializeButtons(upCharacteristics, exit);
    
        protected override void OpenPanel()
        {
            base.OpenPanel();

            moneyCounter.UpdateCounter(PlayerPrefs.GetInt("Money"));
        }

        protected override void OnEnabled()
        {
            base.OnEnabled();

            Player.Instance.Money.OnMoneyCount += moneyCounter.UpdateCounter;
        }

        protected override void OnDisabled()
        {
            base.OnDisabled();

            Player.Instance.Money.OnMoneyCount -= moneyCounter.UpdateCounter;
        }
    }
}
