using UnityEngine;

public class JoystickCanvas : UICanvas
{
    [SerializeField] private Joystick _joystick;

    public static JoystickCanvas Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) 
        {
            Instance = this;
            
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this);
        }
    }

    public Joystick GetJoystick()
    {
        return _joystick;
    }
}
