using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : UICanvas
{
    [SerializeField] CanvasGroup _canvasGroup;
    [SerializeField] Camera _camera;
    [SerializeField] Image _loadingBar;

    private AsyncOperation _asyncOperator;
    private IEnumerator _sceneLoadingCoroutine;
    private IEnumerator _panelAnimationCoroutine;
    private WaitForSecondsRealtime _waitTime = new WaitForSecondsRealtime(0.25f);

    public static Action onLoadGameplayScene;
    public static SceneLoader Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) 
        {
            Instance = this;

            DontDestroyOnLoad(gameObject);        
        }
        else
        {
            Destroy(this);
        }
    }

    public void LoadScene()
    {
        onLoadGameplayScene?.Invoke();

        if (_sceneLoadingCoroutine != null) StopCoroutine(_sceneLoadingCoroutine);
        _sceneLoadingCoroutine = AsyncLoadScene();
        StartCoroutine(_sceneLoadingCoroutine);
    }

    private void StartOpenScene()
    {
        if (_panelAnimationCoroutine != null) StopCoroutine(_panelAnimationCoroutine);
        _panelAnimationCoroutine = OpenScene();
        StartCoroutine(_panelAnimationCoroutine);
    }

    private IEnumerator AsyncLoadScene()
    {
        float loadingProgress;
        _asyncOperator = SceneManager.LoadSceneAsync(1);

        _asyncOperator.allowSceneActivation = false;

        while (!_asyncOperator.isDone)
        {
            loadingProgress = Mathf.Clamp01(_asyncOperator.progress / 0.9f);
            _loadingBar.fillAmount = loadingProgress;

            if (_asyncOperator.progress >= .9f && !_asyncOperator.allowSceneActivation)
            {
                _asyncOperator.allowSceneActivation = true;

                StartOpenScene();
            }
            
            yield return null;
        }

    }

    private IEnumerator OpenScene()
    {
        yield return _waitTime;

        _camera.enabled = false;
        _canvasGroup.DOFade(0, 0.5f).SetUpdate(true);

        yield return _waitTime;

        ClosePanel();
    }
}