using NTC.Global.Cache;
using UnityEngine;
using UnityEngine.UI;

public class UICanvas : MonoCache
{
    [Header("Canvas")]
    [SerializeField] private Canvas _canvas;
    [SerializeField] private GraphicRaycaster _graphicRaycaster;

    protected virtual void OpenPanel()
    {
        _canvas.enabled = true;
        _graphicRaycaster.enabled = true;
    }

    protected virtual void ClosePanel()
    {
        _canvas.enabled = false;
        _graphicRaycaster.enabled = false;
    }

    protected override void OnEnabled()
    {
        base.OnEnabled();

        SceneLoader.onLoadGameplayScene += OpenPanel;
    }

    protected override void OnDisabled()
    {
        base.OnDisabled();

        SceneLoader.onLoadGameplayScene -= OpenPanel;
    }
}
