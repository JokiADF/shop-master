﻿using System;
using CodeBase.Assistants;
using DG.Tweening;
using NTC.Global.Cache;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace CodeBase.Canvas
{
    public class UpgradesUi : MonoCache
    {
        [SerializeField] private RectTransform _panel;
        [SerializeField] private Button _exitButton;
        
        [SerializeField] private Button[] _buttons;
        [SerializeField] private TextMeshProUGUI[] _prices;
        [SerializeField] private Image[] _indicators1;
        [SerializeField] private Image[] _indicators2;
        [SerializeField] private Image[] _indicators3;

        [SerializeField] private Image[] _titleImages;
        [SerializeField] private TextMeshProUGUI _titleText;
        [SerializeField] private Image _lastButtonIcon;
        [SerializeField] private TextMeshProUGUI _lastButtonText;

        [SerializeField] private Sprite _buttonSpriteActiveState;
        [SerializeField] private Sprite _buttonSpriteDeactiveState;

        [SerializeField] private Color _titleColorHrState;
        [SerializeField] private Color _titleColorPlayerState;
        [SerializeField] private Sprite _lastButtonIconSpriteHrState;
        [SerializeField] private Sprite _lastButtonIconSpritePlayerState;

        private Action<CharacteristicsType>[] _action = new Action<CharacteristicsType>[6];
        private UpgradesType _type;

        private void Initialize()
        {
            UpdateButtonValues(_type);
            ChangeUpgradesType(_type);
        }

        internal void InitializeButtons(Action<CharacteristicsType> upCharacteristics, Action exit)
        {
            int amount = _action.Length;

            for (int i = 0; i < amount; i++)
            {
                var index = i;
                
                _action[i] = delegate { upCharacteristics((CharacteristicsType)index); };
            }

            _exitButton.onClick.RemoveAllListeners();
            _exitButton.onClick.AddListener(delegate
            {
                ButtonResize(_exitButton.transform); 
                exit(); 
            });
        }

        internal void OpenPanel(UpgradesType upgradesType)
        {
            UpdateButtonValues(upgradesType);

            if (_type != upgradesType) ChangeUpgradesType(upgradesType);
            
            _panel.DOAnchorPosY(0, 1)
                .SetEase(Ease.Linear);
        }

        private void ChangeUpgradesType(UpgradesType upgradesType)
        {
            _type = upgradesType;
            
            var color = upgradesType == UpgradesType.Assistants
                ? _titleColorHrState
                : _titleColorPlayerState;
            
            for (var i = 0; i < _titleImages.Length; i++)
            {
                _titleImages[i].color = i >= 2 
                    ? color 
                    : new Color(color.r + 34f / 255f, color.g + 34f / 255f, color.b + 34f / 255f);
                
            }

            if (upgradesType == UpgradesType.Assistants)
            {
                _lastButtonIcon.sprite = _lastButtonIconSpriteHrState;
                _lastButtonText.text = "Employ";

                _titleText.text = "Staff";
            }
            else
            {
                _lastButtonIcon.sprite = _lastButtonIconSpritePlayerState;
                _lastButtonText.text = "Income";

                _titleText.text = "Player";
            }
            
            var amount = _buttons.Length;
            
            for (var i = 0; i < amount; i++)
            {
                var indexCharacteristics = upgradesType == UpgradesType.Assistants ? i : i + 3;
                var indexButton = i;
                
                _buttons[i].onClick.RemoveAllListeners();
                _buttons[i].onClick.AddListener(delegate
                {
                    ButtonResize(_buttons[indexButton].transform);
                    _action[indexCharacteristics]((CharacteristicsType)indexCharacteristics);
                    UpdateButtonValues(upgradesType);
                });
            }
        }

        internal void ClosePanel()
        {
            if (_panel.anchoredPosition.y >= 0)
                _panel.DOAnchorPosY(-1115, 1)
                    .SetEase(Ease.Linear);
        }
        
        private void ButtonResize(Transform button)
        {
            button.DOScale(button.localScale * 0.9f, 0.05f)
                .SetEase(Ease.Linear)
                .SetLoops(2, LoopType.Yoyo);
        }

        private void UpdateButtonValues(UpgradesType upgradesType)
        {
            var amount = _buttons.Length;
            var money = PlayerPrefs.GetInt("Money");

            for (var i = 0; i < amount; i++)
            {
                var index = upgradesType == UpgradesType.Assistants ? i : i + 3;
                
                _buttons[i].image.sprite = 
                    money >= PlayerPrefs.GetInt("PriceBuy" + (CharacteristicsType)index) && 
                    PlayerPrefs.GetInt("Level" + (CharacteristicsType)index) < 5
                    ? _buttonSpriteActiveState
                    : _buttonSpriteDeactiveState;

                _prices[i].text = PlayerPrefs.GetInt("Level" + (CharacteristicsType)index) < 5 
                    ? "" + PlayerPrefs.GetInt("PriceBuy" + (CharacteristicsType)index)
                    : "MAX";

                var countActiveIndicators = PlayerPrefs.GetInt("Level" + (CharacteristicsType)index);

                for (var j = 0; j < 5; j++)
                {
                    var color = j < countActiveIndicators
                        ? Color.yellow
                        : Color.white;

                    switch (i)
                    {
                        case 0:
                            _indicators1[j].color = color;
                            break;
                        case 1:
                            _indicators2[j].color = color;
                            break;
                        case 2:
                            _indicators3[j].color = color;
                            break;
                    }
                }
            }
        }

        protected override void OnEnabled()
        {
            base.OnEnabled();

            SceneLoader.onLoadGameplayScene += Initialize;
        }

        protected override void OnDisabled()
        {
            base.OnDisabled();

            SceneLoader.onLoadGameplayScene -= Initialize;
        }
    }
}