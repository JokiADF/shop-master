﻿using System;
using System.Collections;
using System.Collections.Generic;
using NTC.Global.Cache;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public class CustomInvokeRepeating : MonoCache
    {
        private static Dictionary<string, Coroutine> _repeatingCoroutines = new Dictionary<string, Coroutine>();
        
        public static CustomInvokeRepeating Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void InvokeRepeatingWithInt(string key, Action<int> method, int parameters, float delay, float repeatRate)
        {
            if (_repeatingCoroutines.ContainsKey(key))
            {
                Debug.LogError($"A coroutine with key '{key}' is already running. Please use a different key.");
                return;
            }

            var repeatingCoroutine = StartCoroutine(RepeatingCoroutine(method, parameters, delay, repeatRate));
            _repeatingCoroutines.Add(key, repeatingCoroutine);
        }

        public void CancelCustomInvokeRepeating(string key)
        {
            if (_repeatingCoroutines.ContainsKey(key))
            {
                var repeatingCoroutine = _repeatingCoroutines[key];
                StopCoroutine(repeatingCoroutine);
                _repeatingCoroutines.Remove(key);
            }
            else
            {
                Debug.LogWarning($"No coroutine found with key '{key}'.");
            }
        }

        private IEnumerator RepeatingCoroutine(Action<int> method, int parameters, float delay, float repeatRate)
        {
            yield return new WaitForSeconds(delay);

            while (true)
            {
                method.Invoke(parameters);
                yield return new WaitForSeconds(repeatRate);
            }
        }
    }
}