using NTC.Global.Cache;
using UnityEngine;
using UnityEngine.AI;

namespace CodeBase.Customers
{
    public class AIMovement : MonoCache
    {
        [SerializeField] protected NavMeshAgent _agent;
        [SerializeField] protected float _stopDistance = 4;

        internal bool Move(Vector3 targetPosition, float stopDistance = 0)
        {
            var distance = _stopDistance;

            if(stopDistance != 0) distance = stopDistance;

            if(Helper.GetDistance(transform.position, targetPosition) >= distance)
            {
                if(!_agent.enabled) _agent.enabled = true;
                if(_agent.isStopped) _agent.isStopped = false;

                _agent.SetDestination(targetPosition);

                return false;
            }

            if (!_agent.enabled) return true;
            _agent.isStopped = true;
            _agent.enabled = false;

            return true;
        }
    }
}
