using CodeBase.Customers;
using CodeBase.InteractObjects;
using InteractObjects;
using NTC.Global.Cache;
using Tables;
using UnityEngine;

namespace Customers
{
    public class Customer : MonoCache
    {
        [SerializeField] private AIMovement _movement;
        [SerializeField] private CustomerResource _resource;
        [SerializeField] private CustomerMoney _money;
        
        private CashRegister _cashRegister;
        private Table _table;
        private Vector3 _chairPosition;
        private bool _buyState = true;
        private bool _tableState;
        private bool _initialized;

        public CustomerResource Resource => _resource;
    
        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            if (_initialized) return;
            
            _cashRegister = CashRegister.Instance;

            _initialized = true;
        }

        protected override void LateRun()
        {
            if (!_initialized) return;

            if(_buyState)
            {
                if(_movement.Move(_cashRegister.GetPositionForCustomer(this)) && !_resource.MaxResources)
                {
                    _cashRegister.SetCustomer(this);
                }
                else if(_resource.MaxResources && CodeBase.TablesScripts.Tables.Instance.FreeTable)
                {
                    _buyState = false;
                    _tableState = true;
                    _money.SpendMoney(_resource.CountResources * 5, _cashRegister);
                }
            }
            else if(_tableState)
            {
                if(_table == null) 
                {
                    _table = CodeBase.TablesScripts.Tables.Instance.TakeATable(transform.position);
                
                    if(_table != null) _chairPosition = _table.TakeAChair(this);
                }
                else if(_movement.Move(_chairPosition, 0.75f))
                {
                    _tableState = false;
                
                    _resource.PutOnATable(_table);
                    _table.SitAtTheTable(this);
                }
            }
            else if (_table.Unharvested && _movement.Move(EndPositions.Instance.RandomPositionToLeave))
            {
                Despawn();
            }
        }

        private void Despawn()
        {
            _buyState = true;
            _tableState = false;

            _table = null;
            _chairPosition = Vector3.zero;
            
            CustomerSpawner.Instance.RemoveCustomer(this);
        }
    }
}
