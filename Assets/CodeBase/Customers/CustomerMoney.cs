using System;
using CodeBase.InteractObjects;
using InteractObjects;
using NTC.Global.Cache;
using NTC.Global.Pool;
using UnityEngine;

namespace Customers
{
    public class CustomerMoney: MonoCache
    {
        private Money _moneyPrefab;

        private void Start()
        {
            _moneyPrefab = Prefabs.Instance.Money;
        }

        internal void SpendMoney(int amount, CashRegister cashRegister)
        {
            if(amount < 0)
                throw new ArgumentOutOfRangeException();
            
            Money money;

            for(int i = 0; i < amount; i++)
            {
                money = NightPool.Spawn(Prefabs.Instance.Money, transform.position, Quaternion.Euler(0, 45, 0));     

                cashRegister.FoldingMoney(money);
            }
        }
    }
}
