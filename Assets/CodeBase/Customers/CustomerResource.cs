using System;
using System.Collections.Generic;
using CodeBase.Resource;
using NTC.Global.Cache;
using ResourceObjects;
using Tables;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Customers
{
    public class CustomerResource: MonoCache
    {
        [SerializeField] private Transform _placeForResource;
        private int _countWantResources;

        private List<Resource> _resources = new List<Resource>();

        public int CountResources => _resources.Count;
        public bool MaxResources => CountResources == _countWantResources;
    
        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            _countWantResources = Random.Range(1, 5);
        }

        protected override void LateRun()
        {
            for(int i = 0; i < CountResources; i++)
            {
                if(i == 0) _resources[0].GameUpdate(_placeForResource.position);
                else
                {
                    Transform first = _resources[i - 1].transform;
                    Transform second = _resources[i].transform;
                
                    _resources[i].GameUpdate(first.position + Vector3.up);
                }
            }
        }

        internal void PutOnATable(Table table)
        {
            if(CountResources == 0)
                throw new ArgumentOutOfRangeException();

            Resource resource;

            for(int i = 0; i < CountResources; i++)
            {
                resource = _resources[i];

                resource.ResetValues();
                _resources.Remove(resource);

                table.AcceptResource(resource);

                i--;
            }
        }

        public void ReceiveResources(Resource resource)
        {
            if(MaxResources)
                throw new ArgumentOutOfRangeException();

            resource.SetTarget(_placeForResource);
            _resources.Add(resource);
        }
    }
}
