using System;
using System.Collections.Generic;
using NTC.Global.Cache;
using NTC.Global.Pool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Customers
{
    public class CustomerSpawner : MonoCache
    {
        [SerializeField] private float _timeForMinAmount;
        [SerializeField] private float _timeForNormalAmount;
        [SerializeField] private Transform[] _pointsForSpawn;
        
        private float _timerForMinAmount;
        private float _timerForNormalAmount;

        private List<Customer> _customers = new List<Customer>();

        public static CustomerSpawner Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        protected override void LateRun()
        {
            if (_customers.Count < 10)
            {
                _timerForMinAmount += Time.deltaTime;

                if (_timerForMinAmount >= _timeForMinAmount)
                {
                    _timerForMinAmount = 0;
                    
                    SpawnCustomer();
                }
            }
            else if (_customers.Count < 20)
            {
                _timerForNormalAmount += Time.deltaTime;

                if (_timerForNormalAmount >= _timeForNormalAmount)
                {
                    _timerForNormalAmount = 0;
                    
                    SpawnCustomer();
                }
            }
            else
            {
                _timerForMinAmount = 0;
                _timerForNormalAmount = 0;
            }
        }

        private void SpawnCustomer()
        {
            Customer customer = NightPool.Spawn(Prefabs.Instance.Customer, _pointsForSpawn[Random.Range(0, _pointsForSpawn.Length)].position);
            _customers.Add(customer);
        }

        internal void RemoveCustomer(Customer customer)
        {
            if(!_customers.Contains(customer))
                throw new ArgumentOutOfRangeException();

            NightPool.Despawn(customer);
            _customers.Remove(customer);
        }
    }
}
