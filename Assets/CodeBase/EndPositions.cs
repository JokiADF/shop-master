using NTC.Global.Cache;
using UnityEngine;

public class EndPositions : MonoCache
{
    [SerializeField] private Transform[] _endPositions;

    public Vector3 RandomPositionToLeave => _endPositions[Random.Range(0, _endPositions.Length)].position;

    public static EndPositions Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
