﻿using CodeBase.Assistants;
using CodeBase.InteractObjects;
using InteractObjects;
using NTC.Global.Cache;
using UnityEngine;

namespace CodeBase
{
    public class Establishment : MonoCache
    {
        [SerializeField] private Upgrades hr;
        [SerializeField] private Upgrades playerUpgrades;
        [SerializeField] private SpawnerResources spawnerResources;
        [SerializeField] private DestroyerResources destroyerResources;
        [SerializeField] private StorageResources storageResources;
        [SerializeField] private CashRegister cashRegister;

        public Upgrades Hr => hr;
        public Upgrades PlayerUpgrades => playerUpgrades;
        public SpawnerResources SpawnerResources => spawnerResources;
        public DestroyerResources DestroyerResources => destroyerResources;
        public StorageResources StorageResources => storageResources;
        public CashRegister CashRegister => cashRegister;
    
        public static Establishment Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}