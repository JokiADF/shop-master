using UnityEngine;
using UnityEngine.UI;

namespace CodeBase
{
    public class FillAmount : MonoBehaviour
    {
        [SerializeField] private Image _bar;

        private float _value;
        private float _max;
        private float _min;

        public void Initialize(float max)
        {
            _max = max;
            _min = 0;
            _value = 0;

            _bar.fillAmount = 0;
        }

        public void GameUpdate(float value)
        {
            _value = value; 

            GetCurrentFill();
        }

        private void GetCurrentFill()
        {
            var currentOffset = _value - _min;
            var maxOffset = _max - _min;
            var fillAmount = currentOffset / maxOffset;

            _bar.fillAmount = fillAmount;
        }
    }
}
