using TMPro;
using UnityEngine;

namespace CodeBase
{
    public static class Helper
    {
        public static float GetDistance(Vector3 startPosition, Vector3 endPosition)
        {
            return (startPosition - endPosition).sqrMagnitude;
        }

        public static void GetAbbreviatedPrice(TextMeshProUGUI text, int price)
        {
            text.text = "$";

            switch (price)
            {
                case > 0 and <= 999:
                    text.text += price.ToString();
                    break;
                case >= 1000 and <= 9999:
                    text.text += price.ToString();
                    text.fontSize = 4;
                    break;
                case >= 10000 and <= 99999:
                    text.text += Mathf.CeilToInt(price / 1000).ToString() + "K";
                    break;
            }
        }
    }
}
