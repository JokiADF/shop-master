﻿using System;
using DG.Tweening;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public class BuyObjectPanel : BuyPanel
    {
        [SerializeField] private GameObject objectBeforeBuy;

        protected override void Buy()
        {
            transform.DOScale(Vector3.zero, 0.2f)
                .SetEase(Ease.Linear)
                .SetLink(gameObject)
                .OnComplete(Hide);
        }

        protected override void Hide()
        {
            if(objectBeforeBuy.TryGetComponent(out IPurchasingObject purchasingObject))
                purchasingObject.Open();
            else 
                throw new ArgumentException();
            
            base.Hide();
        }
    }
}