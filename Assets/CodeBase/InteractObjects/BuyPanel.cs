using CodeBase.PlayerScripts;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public abstract class BuyPanel : InteractWithMoneyObject
    {
        [SerializeField] private FillAmount fill;
        [SerializeField] private TextMeshProUGUI priceText;
        private static PlayerInteract _playerInteract;

        protected int _startPrice;
        private int _price;
        private int _amountMoney;
        private int _amountMoneyForPlayer;
        private float _timer;
        private float _originalSize;
        private bool _lastMoney;

        private void Start()
        {
            Initialize();
        }

        internal override void Initialize()
        {
            base.Initialize();
            
            UpdatePrice();

            if (_playerInteract == null) _playerInteract = Player.Instance.Interact;
        }

        protected void UpdatePrice()
        {
            _price = _startPrice;
            
            _timer = 0;
            _amountMoney = 0;
            _amountMoneyForPlayer = 0;
            _lastMoney = false;
            
            Helper.GetAbbreviatedPrice(priceText, _price);
            fill.Initialize(_startPrice);
        }

        protected override void Appearance()
        {
            _originalSize = transform.localScale.x;
        }

        protected override void GameUpdate()
        {
            StartInteract(_playerInteract);
        }

        protected override void Interact(WorkerResources workerResources)
        {
            if(PlayerPrefs.GetInt("Money") <= 0 || _lastMoney) return;
        
            if(transform.localScale.x < _originalSize + _originalSize * 0.2f)
                transform.DOScale(Vector3.one * (_originalSize + _originalSize * 0.2f), 0.25f)
                    .SetEase(Ease.Linear);
            
            _amountMoney = (int)(_startPrice / 25f);
            
            if (PlayerPrefs.GetInt("Money") < _amountMoney)
            {
                _amountMoney = PlayerPrefs.GetInt("Money");
            }
            
            if (_price > 0)
            {
                _price -= _amountMoney;
                _amountMoneyForPlayer += _amountMoney;

                if (_timer >= 0.1f || _amountMoneyForPlayer == PlayerPrefs.GetInt("Money"))
                {
                    _playerMoney.SpendMoney(transform.position, _amountMoneyForPlayer);

                    _timer = 0;
                    _amountMoneyForPlayer = 0;
                }
                else
                {
                    _timer += Time.deltaTime;
                }

                fill.GameUpdate(_startPrice - _price);
                Helper.GetAbbreviatedPrice(priceText, _price);
            }
            
            if (_price <= 0 && !_lastMoney)
            {
                _lastMoney = true;
                
                priceText.text = "$0";
                
                _playerMoney.SpendMoney(transform.position, _amountMoneyForPlayer, Buy);
            }
        }
        
        protected virtual void Buy(){}

        protected virtual void Hide()
        {
            Destroy(gameObject);
        }

        protected override void ResetInteractValues()
        {
            base.ResetInteractValues();

            if(!_lastMoney && _originalSize > 0) 
                transform.DOScale(Vector3.one * _originalSize, 0.25f)
                    .SetEase(Ease.Linear);
        }
    }
}
