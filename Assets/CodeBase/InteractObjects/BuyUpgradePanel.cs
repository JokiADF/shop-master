﻿using System;
using DG.Tweening;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public class BuyUpgradePanel : BuyPanel
    {
        private Action _upgrade;
        
        public void SetUpgradeAction(Action upgrade)
        {
            _upgrade = upgrade;
        }
        
        protected override void Buy()
        {
            _upgrade?.Invoke();
        }

        public void UpdatePrice(int price)
        {
            _startPrice = price;
            
            base.UpdatePrice();
        }

        public new void Hide()
        {
            transform.DOScale(Vector3.zero, 0.2f)
                .SetEase(Ease.Linear)
                .SetLink(gameObject)
                .OnComplete(base.Hide);
        }
    }
}