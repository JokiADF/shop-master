using System;
using System.Collections.Generic;
using CodeBase.PlayerScripts;
using Customers;
using DG.Tweening;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public class CashRegister : InteractWithMoneyObject
    {
        [Header("Core")]
        [SerializeField] private StorageResources storageResource;
        [SerializeField] private PlaceForInteract placeForCollectMoney;
        [SerializeField] private Transform placeForFoldingMoney;
        [SerializeField] private float distanceForCollectMoney = 1f;

        [Header("Upgrades")] 
        [SerializeField] private int maxLevelUpgrade = 4;
        [SerializeField] private BuyUpgradePanel buyUpgradePanel;
        [SerializeField] private GameObject cashier;
        
        private int _levelUpgrade;
        private float _income = 1;
        
        public bool Automated { get; private set; }

        private Stack<Money> _activeMoney = new Stack<Money>();
        private List<Customer> _customers = new List<Customer>();

        public static CashRegister Instance;

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            Initialize();
        }

        internal override void Initialize()
        {
            base.Initialize();

            _levelUpgrade = PlayerPrefs.GetInt("CashRegisterLevel");
            UnlockUpgrade();
            buyUpgradePanel.SetUpgradeAction(UpgradeCashRegister);
        }

        protected override void LateRun()
        {
            if(Automated)
                Report(null);
            
            FreeTableCheck();
            base.LateRun();
            GiveOutMoney();
        }

        protected override void Interact(WorkerResources workerResources)
        {
            if (_customers.Count != 0 && storageResource.CountResources > 0)
            {
                if (!_customers[0].Resource.MaxResources)
                    _customers[0].Resource.ReceiveResources(storageResource.GetResource());
                else FreeTableCheck();
            }
        }

        private void UpgradeCashRegister()
        {
            _levelUpgrade++;
            
            if (PlayerPrefs.GetInt("CashRegisterLevel") < _levelUpgrade)
                PlayerPrefs.SetInt("CashRegisterLevel", _levelUpgrade);
            
            UnlockUpgrade();
        }

        private void UnlockUpgrade()
        {
            if (_levelUpgrade >= 1 && !Automated)
            {
                cashier.SetActive(true);
                Automated = true;
            }
            
            switch (_levelUpgrade)
            {
                case 0:
                    buyUpgradePanel.UpdatePrice(50);
                    break;
                case 1:
                    buyUpgradePanel.UpdatePrice(150);
                    break;
                case 2:
                    _income = 2f;
                    buyUpgradePanel.UpdatePrice(250);
                    break;
                case 3:
                    _income = 3f;
                    buyUpgradePanel.UpdatePrice(350);
                    break;
                case 4:
                    _income = 4f;
                    break;
            }
            
            if(_levelUpgrade >= maxLevelUpgrade)
                buyUpgradePanel.Hide();
        }
        
        private void FreeTableCheck()
        {
            if(_customers.Count != 0 
               && _customers[0].Resource.MaxResources 
               && TablesScripts.Tables.Instance.FreeTable)
            {
                _timerForInteract = 0;

                _customers.RemoveAt(0);
            }
        }

        private void GiveOutMoney()
        {
            if(_activeMoney.Count > 0 
               && Helper.GetDistance(placeForCollectMoney.transform.position, _playerTransform.position) <= distanceForCollectMoney)
            {
                if(!placeForCollectMoney.Activate) placeForCollectMoney.Activate = true;

                _playerMoney.ReceiveMoney(_activeMoney.Pop(), _income);
            }
            else if(placeForCollectMoney.Activate) placeForCollectMoney.Activate = false;
        }

        public void FoldingMoney(Money money)
        {
            if(money == null)
                throw new ArgumentNullException();

            _activeMoney.Push(money);

            var position = placeForFoldingMoney.position;
            var count = _activeMoney.Count - 1;

            if(count > 0)
            {
                if(count % 16 < 4)
                {
                    position += new Vector3(0.5f * (count % 16), 0, 0);
                }
                else if(count % 16 < 8)
                {
                    position += new Vector3(0.5f * (count % 16 - 4), 0, -0.5f);
                }
                else if(count % 16 < 12)
                {
                    position += new Vector3(0.5f * (count % 16 - 8), 0, -1);
                }
                else if(count % 16 < 16)
                {
                    position += new Vector3(0.5f * (count % 16 - 12), 0, -1.5f);
                }

                position += new Vector3(0, count / 16 * 0.125f, 0);
            }

            money.transform.DOJump(position, 2, 1, 0.5f)
                .SetEase(Ease.OutQuad);
        }

        public Vector3 GetPositionForCustomer(Customer customer)
        {
            if(customer == null)
                throw new ArgumentNullException();

            var transform1 = transform;
            
            if(_customers.Contains(customer))
            {
                if(_customers.IndexOf(customer) == 0)
                {
                    return transform1.position - transform1.forward;
                }
                else
                {
                    return _customers[_customers.IndexOf(customer) - 1].transform.position - transform1.forward;
                }
            }

            if(_customers.Count == 0) return transform1.position - transform1.forward;
            
            return _customers[^1].transform.position - transform1.forward;
        }

        public void SetCustomer(Customer customer)
        {
            if(_customers.Contains(customer)) return;

            _customers.Add(customer);
        }
    }
}