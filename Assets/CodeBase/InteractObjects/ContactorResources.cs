using System.Collections.Generic;
using CodeBase.PlayerScripts;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public abstract class ContactorResources : InteractObject
    {
        [SerializeField] protected Transform _placeForResource;
        [SerializeField] protected int _maxCount = 5;

        private float _timerForResources;
        protected List<Resource.Resource> _activeResources = new List<Resource.Resource>();

        public int CountResources => _activeResources.Count;

        protected virtual void Start()
        {
            Initialize();
        }

        protected Vector3 GetPositionForResource()
        {
            var position = _placeForResource.position;
            var count = _activeResources.Count;

            if (count <= 0) return position;
            switch(count % 4)
            {
                case 1:
                    position += new Vector3(1, 0, 0);
                    break;
                case 2:
                    position += new Vector3(0, 0, -1);
                    break;
                case 3:
                    position += new Vector3(1, 0, -1);
                    break;
            }

            position += new Vector3(0, count / 4, 0);

            return position;
        }

        protected override void Interact(WorkerResources workerResources)
        {
            _timerForResources += _interactSpeed * Time.deltaTime;

            if(_timerForResources >= _timeForStartInteract)
            {
                _timerForResources = 0;

                InteractWithResource(workerResources);
            }
        }

        protected override void ResetInteractValues()
        {
            base.ResetInteractValues();

            _timerForResources = 0;
        }

        protected virtual void InteractWithResource(WorkerResources workerResources){}
    }
}