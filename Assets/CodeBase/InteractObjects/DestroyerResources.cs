﻿using CodeBase.InteractObjects;
using CodeBase.PlayerScripts;
using CodeBase.Resource;
using DG.Tweening;
using ResourceObjects;

namespace InteractObjects
{
    public class DestroyerResources : ContactorResources
    {
        protected override void InteractWithResource(WorkerResources workerResources)
        {
            if (workerResources.CountResources == 0) return;

            Resource resource = workerResources.GetResources();
            resource.transform.DOJump(_placeForResource.position, resource.transform.position.y + 2f, 1, 0.5f)
                .SetEase(Ease.OutQuad)
                .OnComplete(() => Destroy(resource));
        }
    }
}