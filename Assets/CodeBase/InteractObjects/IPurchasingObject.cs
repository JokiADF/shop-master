﻿using UnityEngine;

namespace CodeBase.InteractObjects
{
    public interface IPurchasingObject
    {
        protected string name { get; set; }
        protected bool isOpen { get; set; }

        public virtual void Open()
        {
            if(PlayerPrefs.GetInt("Open" + name) == 1 && isOpen) return;

            isOpen = true;
            PlayerPrefs.GetInt("Open" + name, 1);
        }
    }
}