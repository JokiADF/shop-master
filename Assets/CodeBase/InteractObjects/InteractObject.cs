using CodeBase.PlayerScripts;
using DG.Tweening;
using NTC.Global.Cache;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public abstract class InteractObject : MonoCache
    {
        [SerializeField] private Transform _placeForStartInteract;
        [SerializeField] protected float _startDistance = 5;
        [SerializeField] protected float _interactSpeed = 5;
        [SerializeField] protected float _timeForStartInteract = 0.5f;
        protected float _timerForInteract;
        
        public Transform PlaceForStartInteract => _placeForStartInteract;
        
        internal virtual void Initialize()
        {
            var interactObjectTransform = transform;
            
            var originalSize = interactObjectTransform.localScale.x;
            interactObjectTransform.localScale = Vector3.zero;
            interactObjectTransform.DOScale(Vector3.one * originalSize, 0.1f)
                .SetEase(Ease.Linear)
                .SetLink(gameObject)
                .OnComplete(Appearance);
        }

        protected virtual void Appearance(){}

        public void StartInteract(WorkerResources workerResources)
        {
            if(Helper.GetDistance(_placeForStartInteract.position, workerResources.transform.position) < _startDistance)
            {
                Report(workerResources);
            }
            else
            {
                ResetInteractValues();
            }
        }

        protected virtual void Report(WorkerResources workerResources)
        {
            PreInteract();
            
            _timerForInteract += _interactSpeed * Time.deltaTime;

            if(_timerForInteract >= _timeForStartInteract)
            {
                Interact(workerResources);
            }
        }

        protected virtual void ResetInteractValues()
        {
            _timerForInteract = 0;
        }

        protected virtual void PreInteract(){}

        protected virtual void Interact(WorkerResources workerResources){}
    }
}