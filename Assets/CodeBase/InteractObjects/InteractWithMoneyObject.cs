﻿using CodeBase.PlayerScripts;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public abstract class InteractWithMoneyObject : InteractObject
    {
        [SerializeField] private PlaceForInteract _placeForVisualInteract;
        
        protected static PlayerMoney _playerMoney;
        protected static Transform _playerTransform;

        internal override void Initialize()
        {
            base.Initialize();

            if(_playerMoney == null) _playerMoney = Player.Instance.Money;
            if(_playerTransform == null) _playerTransform = _playerMoney.transform;
        }
        
        protected override void PreInteract()
        {
            if(!_placeForVisualInteract.Activate) _placeForVisualInteract.Activate = true;
        }

        protected override void ResetInteractValues()
        {
            base.ResetInteractValues();
            
            if(_placeForVisualInteract.Activate) _placeForVisualInteract.Activate = false;
        }
    }
}