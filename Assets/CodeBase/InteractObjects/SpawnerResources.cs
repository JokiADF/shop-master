using CodeBase.PlayerScripts;
using NTC.Global.Pool;
using DG.Tweening;
using UnityEngine;

namespace CodeBase.InteractObjects
{
    public class SpawnerResources : ContactorResources
    {
        [Header("Core")]
        [SerializeField] private Transform[] spawnPoints;
        [SerializeField] private float timeForSpawn = 1;
        
        [Header("Upgrades")]
        [SerializeField] private int maxLevelUpgrade = 3;
        [SerializeField] private BuyUpgradePanel buyUpgradePanel;

        private readonly bool[] _creatingResources = new bool[4];
        private Resource.Resource _createdResource;
        private float _timerForSpawnResources;
        private int _levelUpgrade;

        protected override void Start()
        {
            base.Start();

            _creatingResources[0] = true;
            CustomInvokeRepeating.Instance.InvokeRepeatingWithInt("CreatingResources0",
                CreatingAResource,
                0,
                0,
                timeForSpawn);
                
            _levelUpgrade = PlayerPrefs.GetInt("SpawnerLevel");
            UnlockUpgrade();
            buyUpgradePanel.SetUpgradeAction(UpgradeSpawner);
        }

        protected override void Interact(WorkerResources workerResources)
        {
            if(_activeResources.Count == 0 || workerResources.MaxResources) return;
        
            base.Interact(workerResources);
        }

        protected override void InteractWithResource(WorkerResources workerResources)
        {
            workerResources.SetResources(_activeResources[^1]);
            _activeResources.RemoveAt(index: _activeResources.Count - 1);
        }

        private void CreatingAResource(int index)
        {
            if (CountResources < _maxCount)
            {
                var resource = NightPool.Spawn(Prefabs.Instance.Resource, spawnPoints[index].position);
                resource.transform.DOJump(GetPositionForResource(), 1, 1, 0.5f)
                    .SetEase(Ease.Linear);

                _activeResources.Add(resource);
            }
        }
        
        private void UpgradeSpawner()
        {
            _levelUpgrade++;
            
            if (PlayerPrefs.GetInt("SpawnerLevel") < _levelUpgrade)
                PlayerPrefs.SetInt("SpawnerLevel", _levelUpgrade);
            
            UnlockUpgrade();
        }

        private void UnlockUpgrade()
        {
            if (_levelUpgrade == 0)
            {
                buyUpgradePanel.UpdatePrice(50);
            }
            else
            {
                if (_levelUpgrade >= 1 && !_creatingResources[1])
                {
                    buyUpgradePanel.UpdatePrice(150);
                    _maxCount = 15;

                    _creatingResources[1] = true;
                    CustomInvokeRepeating.Instance.InvokeRepeatingWithInt("CreatingResources1",
                        CreatingAResource,
                        1,
                        0.325f,
                        timeForSpawn);
                }
                if (_levelUpgrade >= 2 && !_creatingResources[2])
                {
                    buyUpgradePanel.UpdatePrice(250);
                    _maxCount = 30;

                    _creatingResources[2] = true;
                    CustomInvokeRepeating.Instance.InvokeRepeatingWithInt("CreatingResources2",
                        CreatingAResource,
                        2,
                        0.134f,
                        timeForSpawn);
                }
                if (_levelUpgrade >= 3 && !_creatingResources[3])
                {
                    _maxCount = 50;

                    _creatingResources[3] = true;
                    CustomInvokeRepeating.Instance.InvokeRepeatingWithInt("CreatingResources3",
                        CreatingAResource,
                        3,
                        0.751f,
                        timeForSpawn);
                }
            }
            
            if(_levelUpgrade >= maxLevelUpgrade)
                buyUpgradePanel.Hide();
        }
    }
}
