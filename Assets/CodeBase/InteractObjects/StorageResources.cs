using CodeBase.PlayerScripts;
using DG.Tweening;
using InteractObjects;
using ResourceObjects;

namespace CodeBase.InteractObjects
{
    public class StorageResources : ContactorResources
    {
        protected override void Interact(WorkerResources workerResources)
        {
            if(_activeResources.Count == _maxCount || workerResources.CountResources == 0) return;

            base.Interact(workerResources);
        }

        protected override void InteractWithResource(WorkerResources workerResources)
        {
            Resource.Resource resource = workerResources.GetResources();
            resource.transform.DOJump(GetPositionForResource(), resource.transform.position.y + 2f, 1, 0.5f)
                .SetEase(Ease.OutQuad);
        
            _activeResources.Add(resource);
        }

        public Resource.Resource GetResource()
        {
            Resource.Resource resource = _activeResources[^1];
            _activeResources.Remove(resource);

            return resource;
        }
    }
}