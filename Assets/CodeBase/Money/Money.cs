using CodeBase.PlayerScripts;
using NTC.Global.Cache;
using NTC.Global.Pool;
using UnityEngine;

public class Money : MonoCache
{
    private Vector3 _startPosition;
    private Transform _target;
    private PlayerMoney _money;
    private float _timer;

    public void StartMoveToThePlayer(Transform target, PlayerMoney money)
    {
        _timer = 0;

        _target = target;
        _startPosition = transform.position;

        _money = money;
        _money.OnMoneyUpdate += GameUpdate;
    }
    
    private new void GameUpdate()
    {
        if(_timer + Time.deltaTime < 1f) 
        {
            _timer += Time.deltaTime * 2;

            float height = _startPosition.y > _target.position.y ? _startPosition.y : _target.position.y;

            transform.position = MathParabola.Parabola(_startPosition, _target.position, 5, _timer);
        }
        else 
        {
            ResetValues();
            Despawn();
        }
    }

    private void ResetValues()
    {
        _money.OnMoneyUpdate -= GameUpdate;
        _money = null;

        _startPosition = Vector3.zero;
        _target = null;
        _timer = 0;
    }

    public void Despawn()
    {
        NightPool.Despawn(this);
    }
}
