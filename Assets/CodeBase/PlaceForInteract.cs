using NTC.Global.Cache;
using UnityEngine;

public class PlaceForInteract : MonoCache
{
    [SerializeField] private SpriteRenderer _outline;
    
    private bool _activate;
    
    public bool Activate 
    {
        get => _activate;
        set
        {
            _activate = value;
            _outline.color = value ? Color.green : Color.white;
        }
    }
}
