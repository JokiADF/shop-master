using NTC.Global.Cache;
using UnityEngine;

namespace CodeBase.PlayerScripts
{
    public class Player : MonoCache
    {
        [SerializeField] private PlayerConfig config;
        [SerializeField] private PlayerCamera playerCamera;
        [SerializeField] private PlayerMovement movement;
        [SerializeField] private PlayerInteract interact;
        [SerializeField] private PlayerMoney money;
        [SerializeField] private PlayerCharacteristics characteristics;

        public PlayerConfig Config => config;
        public PlayerCamera PlayerCamera => playerCamera;
        public PlayerMovement Movement => movement;
        public PlayerInteract Interact => interact;
        public PlayerMoney Money => money;
        public PlayerCharacteristics Characteristics => characteristics;

        public static Player Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null) 
            {
                Instance = this;
            
                DontDestroyOnLoad(gameObject);        
            }
            else
            {
                Destroy(this);
            }
        }
    }
}
