using CodeBase.PlayerScripts;
using UnityEngine;
using NTC.Global.Cache;

public class PlayerCamera : MonoCache
{
    private Transform _target;
    private Vector3 _offset;
    private float _smooth;

    private void Initialize()
    {
        Player player = Player.Instance;

        _target = player.transform;
        _offset = player.Config.Offset;
        _smooth = player.Config.Smooth;

        transform.parent = null;
    }

    protected override void LateRun()
    {
        transform.position = Vector3.Lerp(transform.position, _target.position + _offset, Time.deltaTime * _smooth);
    }

    protected override void OnEnabled()
    {
        base.OnEnabled();

        SceneLoader.onLoadGameplayScene += Initialize;
    }

    protected override void OnDisabled()
    {
        base.OnDisabled();
        
        SceneLoader.onLoadGameplayScene -= Initialize;
    }
}
