﻿using System;
using CodeBase.Assistants;
using NTC.Global.Cache;
using UnityEngine;

namespace CodeBase.PlayerScripts
{
    public class PlayerCharacteristics : MonoCache
    {
        private int _moveSpeedLevel;
        private int _capacityLevel;
        private int _incomeLevel;
        
        private PlayerMovement _movement;
        private PlayerInteract _interact;
        private PlayerMoney _money;

        private void Initialize()
        {
            _movement = Player.Instance.Movement;
            _interact = Player.Instance.Interact;
            _money = Player.Instance.Money;
            
            UpCharacteristics(CharacteristicsType.MoveSpeedPlayer);
            UpCharacteristics(CharacteristicsType.CapacityPlayer);
            UpCharacteristics(CharacteristicsType.Income);
        }

        public void UpCharacteristics(CharacteristicsType type)
        {
            var value = GetUpValuesForCharacteristic(type);

            if (PlayerPrefs.GetInt("Level" + type) <= 0 ||
                PlayerPrefs.GetInt("Level" + type) > 5) 
                return;
            
            switch (type)
            {
                case CharacteristicsType.MoveSpeedPlayer:
                    _movement.UpMoveSpeed(value);
                    break;
                case CharacteristicsType.CapacityPlayer:
                    _interact.UpCapacity(value);
                    break;
                case CharacteristicsType.Income:
                    _money.UpIncome(value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
        
        private float GetUpValuesForCharacteristic(CharacteristicsType type)
        {
            var value = 0f;
        
            switch (type)
            {
                case CharacteristicsType.MoveSpeedPlayer when _moveSpeedLevel >= 5:
                    throw new ArgumentOutOfRangeException();
                case CharacteristicsType.MoveSpeedPlayer:
                    _moveSpeedLevel = PlayerPrefs.GetInt("Level" + CharacteristicsType.MoveSpeedPlayer);

                    value = _moveSpeedLevel switch
                    {
                        1 => 6,
                        2 => 7f,
                        3 => 8f,
                        4 => 9f,
                        5 => 10,
                        _ => value
                    };

                    break;
                case CharacteristicsType.CapacityPlayer when _capacityLevel >= 5:
                    throw new ArgumentOutOfRangeException();
                case CharacteristicsType.CapacityPlayer:
                    _capacityLevel = PlayerPrefs.GetInt("Level" + CharacteristicsType.CapacityPlayer);

                    value = _capacityLevel switch
                    {
                        1 => 3,
                        2 => 5,
                        3 => 7,
                        4 => 10,
                        5 => 15,
                        _ => value
                    };

                    break;
                
                case CharacteristicsType.Income when _incomeLevel >= 5:
                    throw new ArgumentOutOfRangeException();
                case CharacteristicsType.Income:
                    _incomeLevel = PlayerPrefs.GetInt("Level" + CharacteristicsType.Income);

                    value = _incomeLevel switch
                    {
                        1 => 1.1f,
                        2 => 1.2f,
                        3 => 1.3f,
                        4 => 1.4f,
                        5 => 1.5f,
                        _ => value
                    };

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            return value;
        }

        protected override void OnEnabled()
        {
            base.OnEnabled();

            SceneLoader.onLoadGameplayScene += Initialize;
        }

        protected override void OnDisabled()
        {
            base.OnDisabled();
        
            SceneLoader.onLoadGameplayScene -= Initialize;
        }
    }
}