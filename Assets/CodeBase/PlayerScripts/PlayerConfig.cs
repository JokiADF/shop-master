using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "Source/Player/PlayerConfig", fileName = "PlayerConfig", order = 0)]
public class PlayerConfig : ScriptableObject
{
    [Header("[Common]"), Space]
    [SerializeField] private Vector3 _offset = new Vector3(0f, 12f, -8f);
    [SerializeField, Min(0)] private float _smooth = 25;
    [SerializeField, Min(0)] private float _moveSpeed = 3.75f;
    [SerializeField, Min(0)] private int _capacity = 2;
    
    public Vector3 Offset => _offset;
    public float Smooth => _smooth;
    public float MoveSpeed => _moveSpeed;
    public int Capacity => _capacity;
}
