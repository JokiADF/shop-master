using CodeBase.Assistants;
using CodeBase.InteractObjects;
using InteractObjects;
using UnityEngine;

namespace CodeBase.PlayerScripts
{
    public class PlayerInteract : WorkerResources
    {
        private DestroyerResources _destroyer;
        private SpawnerResources _spawner;
        private StorageResources _storage;
        private CashRegister _cashRegister;
        private Upgrades _hr;
        private Upgrades _playerUpgrades;

        private Transform _destroyerTransform;
        private Transform _spawnerTransform;
        private Transform _storageTransform;
        private Transform _cashRegisterTransform;
        private Transform _hrTransform;
        private Transform _playerUpgradesTransform;

        protected override void LateRun()
        {
            if (Establishment.Instance == null) return;

            if (_hr == null)
            {
                _hr = Establishment.Instance.Hr;
                _hrTransform = _hr.PlaceForStartInteract;
            }
            
            if (_playerUpgrades == null)
            {
                _playerUpgrades = Establishment.Instance.PlayerUpgrades;
                _playerUpgradesTransform = _playerUpgrades.PlaceForStartInteract;
            }

            if (_spawner == null)
            {
                _spawner = Establishment.Instance.SpawnerResources;
                _spawnerTransform = _spawner.PlaceForStartInteract;
            }

            if (_destroyer == null)
            {
                _destroyer = Establishment.Instance.DestroyerResources;
                _destroyerTransform = _destroyer.PlaceForStartInteract;
            }

            if (_storage == null)
            {
                _storage = Establishment.Instance.StorageResources;
                _storageTransform = _storage.PlaceForStartInteract;
            }

            if (_cashRegister == null)
            {
                _cashRegister = Establishment.Instance.CashRegister;
                _cashRegisterTransform = _cashRegister.PlaceForStartInteract;
            }

            var transformPosition = transform.position;
        
            if (Helper.GetDistance(transformPosition, _spawnerTransform.position) < 5f)
            {
                _spawner.StartInteract(this);
            }
            else if (Helper.GetDistance(transformPosition, _destroyerTransform.position) < 5f)
            {
                _destroyer.StartInteract(this);
            }
            else if (!_cashRegister.Automated && 
                     Helper.GetDistance(transformPosition, _cashRegisterTransform.position) < 5f)
            {
                _cashRegister.StartInteract(this);
            }
            else if (Helper.GetDistance(transformPosition, _storageTransform.position) < 7f)
            {
                _storage.StartInteract(this);
            }
            else if (_hr != null && Helper.GetDistance(transformPosition, _hrTransform.position) < 7f)
            {
                _hr.StartInteract(this);
            }
            else if (_playerUpgrades != null && Helper.GetDistance(transformPosition, _playerUpgradesTransform.position) < 7f)
            {
                _playerUpgrades.StartInteract(this);
            }
        
            base.LateRun();
        }

        protected override void OnEnabled()
        {
            base.OnEnabled();
    
            SceneLoader.onLoadGameplayScene += Initialize;
        }
    
        protected override void OnDisabled()
        {
            base.OnDisabled();
        
            SceneLoader.onLoadGameplayScene -= Initialize;
        }
    }
}