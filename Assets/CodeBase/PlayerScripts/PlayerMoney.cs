using System;
using DG.Tweening;
using NTC.Global.Cache;
using NTC.Global.Pool;
using UnityEngine;

namespace CodeBase.PlayerScripts
{
    public class PlayerMoney : MonoCache
    {
        private float _income = 1;
        
        private Money _prefab;
        public Action OnMoneyUpdate;
        public Action<int> OnMoneyCount;

        private void Initialize()
        {
            _prefab = Prefabs.Instance.Money;
        }

        protected override void LateRun()
        {
            OnMoneyUpdate?.Invoke();
        }

        public void ReceiveMoney(Money money, float amount = 1)
        {
            if(amount < 0)
                throw new ArgumentOutOfRangeException();

            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + (int)(amount * _income));
            OnMoneyCount?.Invoke(PlayerPrefs.GetInt("Money"));

            money.StartMoveToThePlayer(transform, this);
        }

        public void SpendMoney(Vector3 endPosition, int amount, Action lastMoney = null)
        {
            if(amount < 0)
                throw new ArgumentOutOfRangeException();

            if (PlayerPrefs.GetInt("Money") < amount) return;
            
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") - amount);
            OnMoneyCount?.Invoke(PlayerPrefs.GetInt("Money"));

            Money money = NightPool.Spawn(_prefab, transform.position, Quaternion.Euler(0, 45, 0));
            money.transform.DOJump(endPosition, 2, 1, 0.5f)
                .SetEase(Ease.OutQuad)
                .OnComplete(() =>
                {
                    money.Despawn();
                    lastMoney?.Invoke();
                });
        }

        internal void UpIncome(float value)
        {
            _income = value;
        }

        protected override void OnEnabled()
        {
            base.OnEnabled();

            SceneLoader.onLoadGameplayScene += Initialize;
        }

        protected override void OnDisabled()
        {
            base.OnDisabled();
        
            SceneLoader.onLoadGameplayScene -= Initialize;
        }
    }
}
