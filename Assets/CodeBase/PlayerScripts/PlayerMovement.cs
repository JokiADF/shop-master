using System;
using NTC.Global.Cache;
using UnityEngine;

namespace CodeBase.PlayerScripts
{
    public class PlayerMovement : MonoCache
    {
        [SerializeField] private CharacterController _characterController;

        private Joystick _joystick;
        private float _moveSpeed;
        private bool _isGo;

        public bool IsGo => _isGo;

        private void Initialize()
        {
            _joystick = JoystickCanvas.Instance.GetJoystick();
            _moveSpeed = Player.Instance.Config.MoveSpeed;
        }

        protected override void LateRun()
        {
            CharacterMove();
        }

        internal void UpMoveSpeed(float moveSpeed)
        {
            if (_moveSpeed > moveSpeed) 
                throw new ArgumentOutOfRangeException();

            _moveSpeed = moveSpeed;
        }

        private void CharacterMove()
        {
            var moveVector = Vector3.zero;
            moveVector.x = _joystick.Horizontal * _moveSpeed;
            moveVector.z = _joystick.Vertical * _moveSpeed;

            if (Vector3.Angle(Vector3.forward, moveVector) != 1.0f)
            {
                Vector3 direct = Vector3.RotateTowards(transform.forward, moveVector, _moveSpeed, 0.0f);
                transform.rotation = Quaternion.LookRotation(direct);
            }
            else
            {
                moveVector = Vector3.one;
            }

            if(moveVector == Vector3.zero) _isGo = false;
            else _isGo = true;
        
            _characterController.Move(moveVector * Time.deltaTime);

            transform.position = new Vector3(transform.position.x, 1.25f, transform.position.z);
        }

        protected override void OnEnabled()
        {
            base.OnEnabled();

            SceneLoader.onLoadGameplayScene += Initialize;
        }

        protected override void OnDisabled()
        {
            base.OnDisabled();
        
            SceneLoader.onLoadGameplayScene -= Initialize;
        }
    }
}
