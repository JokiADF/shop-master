﻿using System.Collections.Generic;
using NTC.Global.Cache;
using ResourceObjects;
using UnityEngine;

namespace CodeBase.PlayerScripts
{
    public abstract class WorkerResources : MonoCache
    {
        [SerializeField] private Transform _placeForResource;
        private int _capacity;

        private List<Resource.Resource> _resources = new List<Resource.Resource>();

        public int CountResources => _resources.Count;
        public bool MaxResources => CountResources == _capacity;
    
        protected void Initialize()
        {
            PlayerConfig config = Player.Instance.Config;
            if(_capacity == 0) 
                _capacity = config.Capacity;
        }

        protected override void LateRun()
        {
            int count = _resources.Count;

            for(int i = 0; i < count; i++)
            {
                if(i == 0) _resources[0].GameUpdate(_placeForResource.position);
                else
                {
                    Transform first = _resources[i - 1].transform;
                
                    _resources[i].GameUpdate(first.position + Vector3.up);
                }
            }
        }

        public void SetResources(Resource.Resource resource)
        {
            if(MaxResources && !(Garbage)resource) return;

            resource.SetTarget(_placeForResource);
            _resources.Add(resource);
        }

        public Resource.Resource GetResources()
        {
            Resource.Resource resource = _resources[CountResources - 1];

            resource.ResetValues();
            _resources.Remove(resource);

            return resource;
        }
    
        internal void UpCapacity(float capacity)
        {
            _capacity = (int)capacity;
        }
    }
}