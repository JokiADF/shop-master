using CodeBase.Assistants;
using CodeBase.Resource;
using Customers;
using NTC.Global.Cache;
using ResourceObjects;
using UnityEngine;

public class Prefabs : MonoCache
{
    private const string _pathToResource = "Resource";
    private const string _pathToGarbage = "Garbage";
    private const string _pathToMoney = "Money";
    private const string _pathToCustomer = "Customer";
    private const string _pathToAssistant = "Assistant";

    public Resource Resource => Resources.Load(_pathToResource, typeof(Resource)) as Resource;
    public Garbage Garbage => Resources.Load(_pathToGarbage, typeof(Garbage)) as Garbage;
    public Money Money => Resources.Load(_pathToMoney, typeof(Money)) as Money;
    public Customer Customer => Resources.Load(_pathToCustomer, typeof(Customer)) as Customer;
    public Assistant Assistant => Resources.Load(_pathToAssistant, typeof(Assistant)) as Assistant;
    
    public static Prefabs Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
