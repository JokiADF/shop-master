using NTC.Global.Cache;
using UnityEngine;

public class PreloadScene : MonoCache
{
    private void Start()
    {
        Application.targetFrameRate = 60;
        SceneLoader.Instance.LoadScene();
    }
}
