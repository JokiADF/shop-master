using DG.Tweening;
using NTC.Global.Cache;
using NTC.Global.Pool;
using UnityEngine;

namespace CodeBase.Resource
{
    public class Resource : MonoCache
    {
        private Vector3 _startPosition;
        private float _timer;
        private bool _isCompleteJump;

        public void SetTarget(Transform target)
        {
            transform.DOKill();
            
            var transform1 = transform;
            transform1.parent = target;
            _startPosition = transform1.position;
        }

        public void GameUpdate(Vector3 pos)
        {
            if(!_isCompleteJump)
            {
                if(_timer + Time.deltaTime < 1f) 
                {
                    _timer += Time.deltaTime * 2;

                    transform.position = MathParabola.Parabola(_startPosition, pos, 5, _timer);
                }
                else
                {
                    _isCompleteJump = true;
                    transform.position = pos;
                }
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * 15f);
            }
        }

        public void ResetValues()
        {
            transform.parent = null;
            _isCompleteJump = false;
            _timer = 0;
        }
    
        public void Despawn()
        {
            NightPool.Despawn(this);
        }
    }
}