using System;
using System.Collections.Generic;
using System.Linq;
using CodeBase;
using CodeBase.PlayerScripts;
using CodeBase.Resource;
using Customers;
using DG.Tweening;
using NTC.Global.Cache;
using NTC.Global.Pool;
using ResourceObjects;
using UnityEngine;

namespace Tables
{
    public class Table : MonoCache
    {
        [SerializeField] private GameObject _garbageFake;
        [SerializeField] private Transform _table;
        [SerializeField] private Transform[] _chairs;

        private Customer[] _customersAtTheTable;
        private Garbage _prefabGarbage;
        private PlayerInteract _playerInteract;
        private Transform _playerTransform;
        
        private float _timer;
        private int _amountCustomers;
        private int _maxAmountCustomers;
        
        private bool _unharvested;
        private bool _resourcesOnTheTable;

        private List<Resource> _resources = new List<Resource>();
        private List<Garbage> _garbages = new List<Garbage>();

        public bool Unharvested => _unharvested;
        public bool Activated => gameObject.activeInHierarchy;
        public bool MaxCustomers => _amountCustomers == _maxAmountCustomers;
        public bool PreMaxCustomers => _amountCustomers == _maxAmountCustomers - 1;

        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            _playerTransform = Player.Instance.transform;
            _playerInteract = Player.Instance.Interact;

            _prefabGarbage = Prefabs.Instance.Garbage;
        
            _maxAmountCustomers = _chairs.Length;
            _customersAtTheTable = new Customer[_maxAmountCustomers];
        }

        protected override void LateRun()
        {
            if (_customersAtTheTable.Length > 0 && _customersAtTheTable[0] != null && !_unharvested && _resourcesOnTheTable && CheckCustomer())
            {
                if (_resources.Count > 0)
                {
                    ResourceSpending();
                }
                else
                {
                    ResourcesRanOut();
                }
            }
            else if(_garbages.Count > 0 && Helper.GetDistance(transform.position, _playerTransform.position) < 3)
            {
                Cleaning(_playerInteract);
            }
        }

        private bool CheckCustomer()
        {
            for (int i = 0; i < _amountCustomers; i++)
            {
                if (Helper.GetDistance(_customersAtTheTable[i].transform.position, _chairs[i].position) > 1f) return false;
            }

            return true;
        }
        
        private void ResourceSpending()
        {
            _timer += Time.deltaTime;

            if (_timer > 3)
            {
                _timer = 0;
                    
                _resources[^1].Despawn();
                _resources.Remove(_resources[^1]);
                    
                Garbage garbage = NightPool.Spawn(_prefabGarbage);
                garbage.gameObject.SetActive(false);
                garbage.transform.position = transform.position + Vector3.up * _garbages.Count;
                _garbages.Add(garbage);
            }
        }

        private void ResourcesRanOut()
        {
            int amount = _customersAtTheTable.Length;

            for (int i = 0; i < amount; i++)
            {
                _customersAtTheTable[i] = null;
            }

            _amountCustomers = 0;
                    
            _garbageFake.SetActive(true);
            _resourcesOnTheTable = false;
            _unharvested = true;
        }
        
        public void Cleaning(WorkerResources workerResources)
        {
            if(_garbages.Count == 0)
                throw new ArgumentOutOfRangeException();
            
            Garbage garbage;
            
            for (int i = 0; i < _garbages.Count; i++)
            {
                garbage = _garbages[i];
                
                garbage.gameObject.SetActive(true);
                workerResources.SetResources(garbage);

                _garbages.Remove(garbage);
                
                i--;
            }

            _unharvested = false;
            _garbageFake.SetActive(false);
            CodeBase.TablesScripts.Tables.Instance.ConsiderTheTableClean(this);
        }

        public void AcceptResource(Resource resource)
        {
            resource.transform.DOJump(_table.position + Vector3.up * _resources.Count, resource.transform.position.y + 2f, 1, 0.5f)
                .SetEase(Ease.OutQuad);
        
            _resources.Add(resource);

            _resourcesOnTheTable = true;
        }

        public Vector3 TakeAChair(Customer customer)
        {
            if(MaxCustomers)
                throw new ArgumentOutOfRangeException();

            int count = _chairs.Length;

            for(int i = 0; i < count; i++)
            {
                if(_customersAtTheTable[i] != null) continue;

                _customersAtTheTable[i] = customer;
                _amountCustomers++;

                break;
            }

            return _chairs[_amountCustomers - 1].transform.position - _chairs[_amountCustomers - 1].transform.forward * 1.5f;
        }

        public void SitAtTheTable(Customer customer)
        {
            if (!_customersAtTheTable.Contains(customer))
            {
                Debug.Log("@@@@@@@@@@@@@");
                return;
            }
                // throw new ArgumentOutOfRangeException();

            var rotation = Quaternion.LookRotation(transform.position - customer.transform.position);
            rotation.x = 0;
            rotation.z = 0;

            DOTween.Sequence()
                .Append(customer.transform.DORotateQuaternion(rotation, 0.25f)
                    .SetEase(Ease.Linear))
                .Append(customer.transform.DOMove(_chairs[Array.IndexOf(_customersAtTheTable, customer)].position + Vector3.up, 0.25f)
                    .SetEase(Ease.Linear));
        }
    }
}
