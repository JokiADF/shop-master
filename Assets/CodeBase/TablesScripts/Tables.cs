using System;
using System.Linq;
using NTC.Global.Cache;
using Tables;
using UnityEngine;

namespace CodeBase.TablesScripts
{
    public class Tables : MonoCache
    {
        [SerializeField] private Table[] _tables;
        
        private Table[] _cleanTables;
        private Table[] _dirtyTables;
        
        private int _amountTables;

        private bool _freeTable = true;

        public bool DirtyTable => _dirtyTables.Any(t => t != null && t.Unharvested);
        public bool FreeTable => _freeTable;
    
        public static Tables Instance;

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            Initialize();
        }

        private void Initialize()
        {
            _amountTables = _tables.Length;
            
            _dirtyTables = new Table[_amountTables];
            _cleanTables = new Table[_amountTables];
            
            for (int i = 0; i < _amountTables; i++)
            {
                _cleanTables[i] = _tables[i];
            }
        }

        public Table TakeATable(Vector3 customerPosition)
        {
            Table table = null;
            Table checkTable = null;
            float minDistance = float.MaxValue;
            float distance = 0;

            for(int i = 0; i < _amountTables; i++)
            {
                if(_cleanTables[i] == null || !_cleanTables[i].Activated || _cleanTables[i].MaxCustomers) continue;
                else if(_cleanTables[i].Unharvested)
                {
                    _dirtyTables[Array.IndexOf(_dirtyTables, null)] = _cleanTables[i];
                    _cleanTables[i] = null;

                    continue;
                }
                if (checkTable == null) checkTable = _cleanTables[i];

                distance = Helper.GetDistance(customerPosition, _cleanTables[i].transform.position);
                if (distance < minDistance)
                {
                    table = _cleanTables[i];
                    minDistance = distance;
                }
                else checkTable = _cleanTables[i];
            }

            if (table == null || checkTable == table)
            {
                _freeTable = false;
            }

            return table;
        }

        public Table ClearTheTable(Vector3 position) 
        {
            if (!DirtyTable)
                throw new NullReferenceException();
            
            Table table = null;
            float minDistance = float.MaxValue;
            float distance = 0;

            for(int i = 0; i < _amountTables; i++)
            {
                if(_dirtyTables[i] == null) continue;
                else if (!_dirtyTables[i].Unharvested)
                {
                    _cleanTables[Array.IndexOf(_cleanTables, null)] = _dirtyTables[i];
                    _dirtyTables[i] = null;
                    
                    continue;
                }

                distance = Helper.GetDistance(position, _dirtyTables[i].transform.position);
                if (distance < minDistance)
                {
                    minDistance = distance;

                    table = _dirtyTables[i];
                }
            }

            if (table != null)
            {
                _dirtyTables[Array.IndexOf(_dirtyTables, table)] = null;
            }
            
            return table;
        }

        internal void FreedTheTable()
        {
            Table checkTable = null;

            for(int i = 0; i < _amountTables; i++)
            {
                if(_cleanTables[i] == null || !_cleanTables[i].Activated || _cleanTables[i].MaxCustomers) continue;
                
                _freeTable = true;
                
                return;
            }

            _freeTable = false;
        }

        internal void ConsiderTheTableClean(Table table)
        {
            if(_cleanTables.Contains(table) || table.Unharvested) return;

            _cleanTables[Array.IndexOf(_cleanTables, null)] = table;
            _freeTable = true;
        }
    }
}
