﻿// ----------------------------------------------------------------------------
// The MIT License
// NightPool is an object pool for Unity https://github.com/MeeXaSiK/NightPool
// Copyright (c) 2021-2022 Night Train Code
// ----------------------------------------------------------------------------

using UnityEngine;

namespace NTC.Global.Pool
{
    public class NightPoolEntry : MonoBehaviour
    {
        [SerializeField] private PoolPreset poolPreset;

        public static NightPoolEntry Instance { get; private set; }

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;

                DontDestroyOnLoad(gameObject);

                NightPool.InstallPoolItems(poolPreset);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            NightPool.Reset();
        }
    }
}